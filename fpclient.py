#!/usr/bin/env python3
"""
(C) 2019 Wilbert van Ham, Radboud University
released under General Public License version 3 (GPLv3)

This client library is different from the python 2 version in that is 
does not do extrapolation. It only saves the last position
"""
import socket
import sys 
import binascii 
import struct
import time
import threading
import warnings
import math
import logging
import numpy as np
import copy
import weakref

class FpClient(object):
	"""
	Client for NDI First Principles
	"""
	## package and component types
	pTypes = ['Error', 'Command', 'XML', 'Data', 'Nodata', 'C3D']
	cTypes = ['', '3D', 'Analog', 'Force', '6D', 'Event']
	
	def __init__(self, host="localhost", port=3020):	
		self.p = [] # positions
		self.t = float("nan") # first principles server time
		self.ta = float("nan") # arrival time
		
		self.stoppingStream = False;
		self.win32TimerOffset = time.time()
		time.clock() # start time.clock 
	
		self.host = host
		self.port = port
		# Create a socket (SOCK_STREAM means a TCP socket)
		self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		# socket without nagling
		self.sock.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)
		# Connect to server and send data
		self.sock.settimeout(3)
		try:
			self.sock.connect((host, port))
		except Exception as e:
			logging.error("ERROR connecting to FP Server: "+str(e))
			raise

		self.sendCommand("SetByteOrder BigEndian")
		self.receive()



	# low level communication functions
	def send(self, body, pType):
		"""
		type  0: error, 1: command, 2: xml, 3: data, 4 nodata, 5: c3d
		pack length and type as 32 bit big endian.
		"""
		if isinstance(body, str):
			body = body.encode() # defaults to utf-8
		head = struct.pack(">I", len(body)+8) + struct.pack(">I", pType)
		logging.debug("Sent {}: Body ({}): {}".format(self.pTypes[pType], len(body), body))
		try:
			self.sock.sendall(head+body)
		except Exception as e:
			logging.error("ERROR sending to FP server: %s", e)

	def sendHandshake(self):
		head = struct.pack(">I", 0) + struct.pack("BBBB", 1,3,3,6)
			
	def sendCommand(self, body):
		self.send(body, 1)
		
	def sendXml(self, body):
		self.send(body, 2)

	def receive(self):
		"""
		set pSize, pType and pContent
		"""
		try:
			# Receive data from the server and shut down
			# the size given is the buffer size, which is the maximum package size
			t0 = time.time()
			self.pSize = struct.unpack(">I", self.sock.recv(4))[0] # package size in bytes
			self.pType = struct.unpack(">I", self.sock.recv(4))[0]
			logging.debug("Received ({:6.3f} s): {}, {})".
				format(time.time()-t0, self.pSize, self.pTypes[self.pType]))
			if self.pSize>8:
				self.pContent = self.sock.recv(self.pSize-8)
			else:
				self.pContent = bytes()
			if logging.getLogger().isEnabledFor(logging.DEBUG):
				self.show()
			return self.pType
		except Exception as e:
			logging.error("ERROR receiving from FP server: %s", e)
			self.pType = 0 # error
			return self.pType

	def show(self):
		"""
		Show last received package. uses: pType and pContent
		"""
		print("Received: {} ({})".format(self.pTypes[self.pType], self.pType))
		if self.pType in [0,1]: # error or command
			print("  Command: #{}#".format(self.pContent))
			#print "  Received: #{}#".format(binascii.hexlify(self.pContent))
		elif self.pType in [3]: # data
			cCount = struct.unpack(">I", self.pContent[0:4])[0]
			pointer = 4
			print("  {} data components".format(cCount))
			for i in range(cCount):
				[cSize, cType, cFrame, cTime] = struct.unpack(">IIIQ", self.pContent[pointer:pointer+20])
				pointer += 20
				print("  component size: {}\n  component type: {} ({})\n  component frame: {}\n  component time: {}".format(cSize, self.cTypes[cType], cType, cFrame, cTime/1E6))
				if cType == 1: # 3D
					[mCount] = struct.unpack(">I", self.pContent[pointer:pointer+4])
					pointer+=4
					for j in range(mCount):
						[x, y, z, delta] = struct.unpack(">ffff", self.pContent[pointer:pointer+16])
						pointer+=16
						print("  marker[{}]: ({}, {}, {}) +/- {} m".format(j, x/1E3, y/1E3, z/1E3, delta/1E3))
				elif cType == 2: # analog
					[cCount] = struct.unpack(">I", self.pContent[pointer:pointer+4])
					pointer+=4
					for j in range(cCount):
						[v] = struct.unpack(">f", self.pContent[pointer, pointer+4])
						pointer += 4
						print("channel[{}]: {} V".format(j, v))
				elif cType == 4: # 6D
					[tCount] = struct.unpack(">I", self.pContent[pointer, pointer+4])
					pointer+=4
					for j in range(tCount):
						[q0, qx, qy, qzx, y, z, delta] = struct.unpack(">fffffff", self.pContent[pointer:pointer+28])
						pointer+=28
						print("  tool[{}]: ({}, {}, {}, {})({}, {}, {}) +/- {} m".format(j, x/1E3, y/1E3, z/1E3, delta/1E3))
			print("  Received data: #{}#".format(binascii.hexlify(self.pContent)))
		else:
			print("  Other {}".format(self.pContent))
			print("  Received: #{}#".format(binascii.hexlify(self.pContent)))
			
	def parse3D(self):
		"""
		Parse 3D data components in the last received package,
		return them as n x 3 array and an 
		"""
		if self.pType == 3: # data
			cCount = struct.unpack(">I", self.pContent[0:4])[0]
			pointer = 4
			markerList = []
			for i in range(cCount):
				[cSize, cType, cFrame, cTime] = struct.unpack(">IIIQ", self.pContent[pointer:pointer+20])
				pointer += 20
				if cType == 1: # 3D
					# number of markers
					[mCount] = struct.unpack(">I", self.pContent[pointer:pointer+4])
					pointer+=4
					for j in range(mCount):
						[x, y, z, delta] = struct.unpack(">ffff", self.pContent[pointer:pointer+16])
						pointer += 16
						markerList.append([x, y, z])
			return (np.array(markerList)*1e-3, cTime*1e-6)
		else:
			logging.error("expected 3d data but got packageType: {}".format(self.pType))
			if self.pType==1:
				logging.info("  package: ", self.pContent)
			return np.matrix([])
			
	def time(self):
		if sys.platform == "win32":
			# on Windows, the highest resolution timer is time.clock()
			# time.time() only has the resolution of the interrupt timer (1-15.6 ms)
			# Note that time.clock does not provide time of day information. 
			# EXPECT DRIFT if you do not have ntp better than the MS version
			return self.win32TimerOffset + time.clock()
		else:
			# on most other platforms, the best timer is time.time()
			return time.time()

	# background thread functions
	def startThread(self):
		# I dont care about warnings that my markers are not moving
		warnings.simplefilter('ignore', np.RankWarning)
		logging.info("starting client thread")
		self.sendCommand("StreamFrames FrequencyDivisor:1")
		self.receive()

		# main data retrieval loop
		tSync = 0
		nSync = 0
		while not self.stoppingStream:
			retval = self.receive()
			if retval != 3: # not data
				logging.info("not data: {}".format(self.pTypes[retval]))
				continue
			(self.p, self.t) = self.parse3D() # position of markers and fp server time
			self.ta = self.time() # arrival time
						
		#self.sendCommand("Bye")
		self.stoppingStream = False
		
	def __enter__(self):
		"""
		Start the background thread that synchronizes with the fp server
		"""
		logging.info("starting")
		self.thread = threading.Thread(target = self.startThread)
		self.thread.start()

		# wait for up to 3 s for sensible data
		for i in range(30):
			logging.debug("waiting for data from FP server: %i", i)
			if not math.isnan(self.t):
				break
			time.sleep(0.1)
		return self
		
	def __exit__(self, type, value, traceback):

		"""
		Stop the background thread that synchronizes with the fp server
		"""
		if(self.thread.isAlive()):
			self.stoppingStream = True
			logging.info("stopping")
			self.thread.join() # wait for it to stop
			logging.info("stopped")

			
	def getPosition(self):
		"""
		synchronous call for positions, retry a few times in case of failure
		"""
		retval = 0 # error

		for i in range(30):
			self.sendCommand("SendCurrentFrame 3D Analog")
			retval = self.receive()

			if retval != 3: # not data
				logging.info("not data: {}".format(self.pTypes[retval]))
			else:
				(self.p, self.t) = self.parse3D() # position of markers and fp server time
				return self.p
			time.sleep(0.1)
		logging.error("not data")
		return np.array([])


if __name__ == '__main__':
	#logging.basicConfig(level=logging.INFO)
	server = "localhost"
	if len(sys.argv) > 1:
		server = sys.argv[1]
		
	print ("fp server: ", server)

	client = FpClient(server)      # make a new NDI First Principles client
	print(client.getPosition()[0]) # get marker position
	with client:                   # make a background stream that receives data
		for i in range(3):
			print(client.p[0])
			time.sleep(1)
