# First Principles Client

Client for NDI First Principle in pure Python 3.

Before you can use this client library, make sure that NDI first principles is running on the computer named 'optotrak'. Make sure that fpclient.py is in a place where python can find it, for instance in the same directory as the experiment script.

To get the position of the first marker synchronously, that is waiting for the measurement to arrive:

    import fpclient
    optotrak = fpclient.FpClient('optotrak')
    print(optotrak.getPosition()[0])

To get the position and time of the first marker asynchronously, that is the last measured position:

    import fpclient
    with fpclient.FpClient('optotrak') as optotrak:
        print(optotrak.p[0], optotrak.t)
        
If you make this call quickly, make sure to allow the background thread some time too:

    import time
    import fpclient
    with fpclient.FpClient('optotrak') as optotrak:
        while True:
            time.sleep(0) # avoid GIL
            print(optotrak.p[0], optotrak.t)


The module supports standard logging functionality:

    import logging
	logging.basicConfig(level=logging.INFO)

